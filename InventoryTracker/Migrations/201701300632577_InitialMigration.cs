namespace InventoryTracker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "userName", c => c.String(nullable: false, maxLength: 450));
            CreateIndex("dbo.Users", "userName", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Users", new[] { "userName" });
            AlterColumn("dbo.Users", "userName", c => c.String(nullable: false));
        }
    }
}

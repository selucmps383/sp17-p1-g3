namespace InventoryTracker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SecondMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "password", c => c.String(nullable: false));
            DropColumn("dbo.Users", "passsword");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "passsword", c => c.String(nullable: false));
            DropColumn("dbo.Users", "password");
        }
    }
}

namespace InventoryTracker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ThirdMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "confirmpassword", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "confirmpassword", c => c.String(nullable: false));
        }
    }
}

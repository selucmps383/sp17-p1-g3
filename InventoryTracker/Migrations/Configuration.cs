namespace InventoryTracker.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<InventoryTracker.Models.InventoryTrackerContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(InventoryTracker.Models.InventoryTrackerContext context)
        {
            context.Users.AddOrUpdate(u => u.id, new User() { userName = "admin", password = System.Web.Helpers.Crypto.HashPassword("selu2017"), confirmpassword = System.Web.Helpers.Crypto.HashPassword("selu2017"), firstName = "Admin", lastName = "Admin", isAdmin=true });
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InventoryTracker.Models
{
    public class InventoryItem
    {
        public int id { get; set; }

        [ForeignKey("CreatedByUser")]
        public int CreatedByUserID { get; set;}

        public virtual User CreatedByUser { get; set; }

        public string name { get; set; }

        public int quantity { get; set; }

        public decimal price { get; set; }


    }
}
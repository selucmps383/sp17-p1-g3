﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace InventoryTracker.Models
{
    public class InventoryTrackerContext : DbContext
    {
        public InventoryTrackerContext() : base("name=InventoryTrackerContext")
        {

        }

        public DbSet<User> Users {get;set;}

        public DbSet<InventoryItem> InventoryItems { get; set; }


    }
}
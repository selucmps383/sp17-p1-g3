﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace InventoryTracker.Models
{
    public class User
    {
        [Required]
        [Index(IsUnique =true)]
        [Key]
        public int id { get; set; }

        [Required]
        [Index(IsUnique = true)]
        [StringLength(450)]
        [Display(Name ="User Name")]
        public string userName { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string firstName { get; set;}
        [Required]
        [Display(Name = "Last Name")]
        public string lastName { get; set;}
        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string password { get; set; }
    
        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        public string confirmpassword { get; set; }
        [Required]
        public Boolean isAdmin { get; set; }

        public virtual ICollection<InventoryItem> InventoryItems { get; set; }

    }


}